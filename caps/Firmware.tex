\chapter{Plataforma}
\label{cap:plataforma}


\section{Sistema Operativo}

Un sistema operativo es un programa que soporta las funciones básicas de una computadora y provee servicios a otras aplicaciones que proveen las funcionalidades que el usuario quiere o necesita y corren en el mismo sistema.
Los servicios que provee el sistema operativo ayudan a escribir aplicaciones de forma más rápida, simple y mantenible.

\subsection{Sistema Operativo en Tiempo Real}

La mayoría de los sistemas operativos aparentan permitir que múltiples programas se ejecutan en simultaneo.
A esto se lo denomina multitareas, o \textit{multitasking}.
En realidad, cada núcleo de cada procesador puede correr un solo hilo de ejecución en un cierto momento de tiempo.
Una pieza del sistema operativo llamada \textit{scheduler} es responsable de decidir que programa correr y cuando hacerlo, proveyendo la ilusión de ejecuciones simultaneas mediante un cambio veloz entre programas.

Los sistemas operativos se clasifican en función de como el scheduler decide cuál programa ejecutar y cuando hacerlo.
Por ejemplo, el scheduler usado en un sistema operativo multi usuario, como Unix, se asegurará que cada usuario disponga de un tiempo de procesamiento justo.

El scheduler en un Sistema Operativo en Tiempo Real RTOS\footnote{Del inglés: Real Time Operative System} está diseñado para proveer un patrón de ejecuciones predecible, normalmente llamado determinismo.
Esto es de particular interés en los sistemas embebidos, ya que estos normalmente requieren operar en tiempo real.
La operación en tiempo real especifica que el sistema debe responder a un cierto evento dentro de un tiempo definido.
Esto solo se puede garantizar si el comportamiento del scheduler del sistema operativo se comporta de una forma predecible, y por ende deterministica.

Los scheduler de los sistemas operativos en tiempo real logran determinismo permitiendo al desarrollador asignar prioridades a cada proceso de ejecución.
Esta prioridad es utilizada para conocer cual hilo de ejecución correr a continuación.

A los hilos de ejecución comandados por el RTOS se los denomina generalmente tareas, o \textit{tasks}.
Cada tarea se ejecuta en su propio contexto sin dependencia directa con las demás que se ejecuten en el sistema ni con el scheduler mismo.


\subsection{Política del Scheduler}

El nucleo, o \textit{kernel}, puede suspender y luego resumir una tarea muchas veces durante su tiempo de ejecución.

La política del scheduler es el algoritmo usado para decidir cual tarea ejecutar en un momento en particular. 
El funcionamiento de un sistema multi usuario (no en tiempo real) normalmente permitirá a cada tarea una porción ``justa'' de tiempo de procesamiento.
Ésto no es lo que sucede en sistemas operativos de tiempo real.

Además de ser suspendidas involuntariamente por el kernel, una tarea es capaz suspenderse a si misma.
Esto se puede utilizar en caso de que se necesite una demora por un período específico de tiempo o \textit{delay}, porque se necesita esperar que un recurso se encuentre disponible o que ocurra un evento específico, como la pulsación de un botón.
Una tarea bloqueada o durmiendo no es capaz de ejecutarse y no se le asignará tiempo de ejecución del procesador.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{preemptive}
	\caption{Flujo de tareas en un CPU}
	\label{fig:preemptive}
\end{figure}

Analizando el flujo de un RTOS como el de la Fig. \ref{fig:preemptive}, se destacan los siguientes eventos:
\begin{enumerate}
	\item La tarea 1 está corriendo.
	\item El kernel suspende la tarea 1.
	\item La tarea 2 es resumida.
	\item Mientras la tarea 2 se ejecuta, bloquea un periférico del procesador para su uso exclusivo.
	\item El kernel suspende la tarea 2.
	\item La tarea 3 es resumida.
	\item La tarea 3 intenta utilizar el mismo recurso que la tarea 2 bloqueó. Al no estar disponible, se suspende a sí misma.
	\item El kernel resume la tarea 1.
	\item Al correr nuevamente la tarea 2, se termina de usar el periférico bloqueado y lo libera.
	\item Una nueva ejecución de la tarea 3 encuentra el periférico liberado por lo que se puede ejecutar hasta que sea suspendida por el kernel.
\end{enumerate}



\section{Controlador de Vuelo}
El controlador de vuelo es un sistema que permite al UAV mantenerse estable sin necesidad de intervención humana constante.

Se puede pensar en el controlador de vuelo como un sistema, el cual posee como entrada los datos de las mediciones correspondientes a la posición y orientación actual. A la salida se obtiene las señales de control necesarias para que los propulsores puedan mantener al sistema en un vuelo estable.

\subsection{Arquitectura del sistema} 
Como núcleo del sistema de control del vehículo se utiliza un kernel en tiempo real denominado FreeRTOS\footnote{https://www.freertos.org/}, el cual nos provee una plataforma abierta, altamente probada y de amplia aceptación en el mercado.

El sistema está dividido en servicios, como se puede observar en la Fig. \ref{fig:fw_flow}.
Cada servicio corre un bucle en tareas separadas y con prioridades adecuadas. A su vez, cada bucle cumple funciones específicas como la lectura de los diferentes sensores, el cálculo de los filtros, la actualización de los motores, etc.
Para lograr esto, los servicios se comunican con los componentes de hardware mediante controladores específicos de cada sensor o actuador involucrado en el proceso.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{firmware_flow}
	\caption{Diagrama general de la arquitectura del sistema}
	\label{fig:fw_flow}
\end{figure} 

\subsection{Controladores}
Cada componente que conforma al UAV fue diseñado y producido por empresas distintas. 
A su vez, cada uno posee diferentes protocolos de comunicación, y es necesario manejarlos de diferentes formas según la función que cumplan y las especificaciones del fabricante.
Debido a esto, existe una capa intermedia entre el hardware y los servicios que provee la abstracción necesaria para poder utilizar los periféricos de manera mas sencilla.

Estos controladores, normalmente llamados \textit{drivers}, permiten al desarrollador comunicarse con cualquier módulo sin necesidad de enfocarse en el protocolo utilizado ni en el tipo de información a enviar, facilitando el desarrollo y brindando la ventaja de poder realizar modificaciones en los sensores sin tener que modificar la utilización de estos.
El único requisito es seleccionar el driver adecuado.

\subsection{Servicios}

El sistema cuenta con diferentes servicios para todas las tareas que debe efectuar durante el vuelo. 
Entre ellos, podemos destacar los encargados de leer cada uno de los sensores, enviar órdenes a los motores y hasta la simple tarea de modificar los indicadores luminosos según el estado.

La tarea principal, llamada Hombre en el medio o MITM\footnote{Del inglés: Man in the middle.}, es la encargada de recibir las actualizaciones por medio de una cola de eventos y actuar en consecuencia, según el tipo de evento que haya sido recibido.
En la Fig. \ref{fig:mitm} se observa con detalle el flujo de funcionamiento de esta tarea. Se puede apreciar en el diagrama que esta tarea cumple un rol fundamental en el sistema de control del UAV y por ende una falla en la misma sería crítica para el funcionamiento del mismo. La misma se ejecuta a una tasa de 300 veces por segundo.


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{mitm}
	\caption{Detalle del servicio MITM}
	\label{fig:mitm}
\end{figure}

Los eventos principales se dividen en 3 grupos:
\begin{itemize}
\item{IMU\_UPDATE}

Cuando el servicio encargado de obtener las lecturas de los sensores de la unidad de medición inercial posee datos nuevos, los empaqueta en una instancia de la estructura \textit{imuData\_t} y los despacha por la cola \textit{xQueueEventHandler} para ser procesados por el servicio MITM.
El bucle principal, luego de recibir este evento, se encarga de actualizar el estado del filtro Kalman\footnote{Ver Cap. \ref{cap:control} Sec. \ref{cap:control:kalman}}.
Luego de obtener los valores de actitud\footnote{Estado instantaneo de orientación y movimiento del vehículo.} actualizados, vuelve a calcular el \textit{PID}\footnote{Filtro Proporcional-Integrativo-Derivativo. Ver Cap. \ref{cap:control} Sec. \ref{sec:PID}} y realimentar los motores.

\item{JOY\_UPDATE}

Al enviar una orden desde un comando externo, como puede ser una computadora, el servidor de CoAP\footnote{Ver Cap. \ref{cap:comunicacion} Sec. \ref{sec:coap}} procesa el pedido y lo despacha a la función correspondiente. 
En este caso, al recibir una solicitud de \textit{joystick}, el servicio decodifica el cuerpo del mensaje para convertirlo en un mensaje de \textit{udpData\_t} y lo envía a la cola xQueueEventHandler para ser procesado por el bucle principal.
Una vez recibido el mensaje, el servicio MITM obtiene un nuevo \textit{setpoint} sobre el cual calcular el error a corregir con el PID.

\item{PID\_UPDATE}

Para facilitar el proceso de calibración, se puso a disposición del usuario la posibilidad de enviar los parámetros de calibración del filtro PID a través de una transacción CoAP.
Cuando un mensaje es recibido en la \textit{URI}\footnote{Del inglés: Uniform Resource Identifier. Identificador Único de Recursos.}, se reemplazan los valores actuales por los nuevos recibidos, los cuales son utilizados en el cálculo de compensación inmediatamente.

\item{HEIGHT\_UPDATE}

Similar a IMU\_UPDATE, al recibir una actualización de altura proveniente del sensor láser el servicio MITM, se procede a recalcular el filtro Kalman.

\end{itemize}

En el apéndice \ref{apd:Estructuras de Datos} se encuentra la descripción de las estructuras de datos implicadas en los servicios antes mencionados.

Una vez estimado el filtro Kalman y obtenido los valores de salida del filtro PID para la corrección del ángulo de \textit{roll}\footnotemark, \textit{pitch}\footnotemark[\value{footnote}] y \textit{yaw}\footnotemark[\value{footnote}], se calcula el valor de empuje necesario en cada motor según la disposición y giro de los mismos. 
Utilizando la numeración propuesta para los motores (Fig. \ref{fig:motores}), el empuje necesario en cada motor para corregir la actitud del vehículo se calcula de la siguiente forma:

\footnotetext{Ver Fig. \ref{fig:ejesUAV}.}


\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{motores}
	\caption{Numeración de los motores en el UAV}
	\label{fig:motores}
\end{figure}

\begin{itemize}
        \item Empuje motor 1 = throttle - pitch - roll - yaw
        \item Empuje motor 2 = throttle - pitch + roll + yaw
        \item Empuje motor 3 = throttle + pitch + roll - yaw
        \item Empuje motor 4 = throttle + pitch - roll + yaw
\end{itemize}



Donde \textit{throttle} es el valor de empuje base enviado mediante el control remoto para el vuelo del vehículo.

\section{Telemetría}

Dado que el vehículo está diseñado para operar de manera autónoma y remota, es necesario contar con un sistema de obtención de datos para poder controlar y observar el estado del UAV durante el vuelo.
Para ello, mediante el uso del protocolo CoAP, el sistema envía mensajes con información sobre el estado y el valor de sus variables a un servidor que se encarga de recibirlos, interpretarlos y almacenarlos para ser consultados en cualquier momento durante o luego del vuelo.

Los parámetros más importantes son los siguientes:
\begin{itemize}
	\item Tiempo de vuelo.
	\item Mediciones de los sensores.
	\item Resultado de la estimación del filtro Kalman.
	\item Valores de throttle de los propulsores.
\end{itemize}

Debido a la frecuencia con la que las variables son actualizadas, y el costo de enviar la información de forma inalámbrica, se aplica una reducción a la tasa de muestreo, resultando en una frecuencia de muestreo de $10Hz$, la cual es algunos órdenes de magnitud menor que la tasa de cálculo del bucle principal.
Esta velocidad es lo suficientemente lenta para no interferir con el funcionamiento del sistema, pero es suficiente para brindar la información necesaria para un correcto análisis del estado y comportamiento del vehículo.

Para ver el listado de los diferentes tipos de mensajes enviados por telemetría, referirse al apéndice \ref{apd:Estructuras de Datos}, sección \ref{ref:telemetria}.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{grafana_accel}
	\caption{Grafico de los valores del Acelerómetro.}
	\label{fig:grafana_accel}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.75\textwidth]{grafana_gyro}
\caption{Grafico de los valores del Giroscopio.}
\label{fig:grafana_gyro}
\end{figure}


\subsection{Visualización}
Para una fácil visualización de los datos recolectados, se generaron paneles interactivos utilizando una herramienta de software libre llamada Grafana\footnote{https://grafana.com/}.
Este motor permite realizar distintos tipos de gráficos, como los observados en las Fig \ref{fig:grafana_accel}, \ref{fig:grafana_gyro} y \ref{fig:grafana_motors}, facilitando la interpretación de los valores y el acceso a datos de cualquier período de tiempo.


\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{grafana_motors_gauge}
\caption{Grafico de los valores instantáneos de cada motor.}
\label{fig:grafana_motors}
\end{figure}
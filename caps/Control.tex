%Capitulo 1 - Introducción
\chapter{Control}
\label{cap:control}

\section{Actitud del Vehículo}

El reciente interés en diseñar y volar pequeños UAVs ha estimulado investigaciones sobre control y navegación de dichos vehículos.
El potencial para naves pequeñas de bajo costo es amplio: reconocimiento, vigilancia, búsqueda y rescate, sensado remoto, monitoreo del tráfico, asistencia en desastres naturales, etc.
Una pieza fundamental en estas misiones es la navegación precisa del vehículo.
Típicamente, los pequeños UAVs son difíciles de volar sin un piloto entrenado.

Para un vehículo aéreo no tripulado, la habilidad de estimar la orientación o actitud de forma precisa es crucial para aplicaciones como mapeo, localización, o usos de realidad aumentada.
Pero es aún más importante determinar la orientación para lograr una correcta estabilización del vehículo en el aire, donde incluso pequeños errores de actitud pueden llevar a fallas de mediciones y pérdidas de la estabilidad.

La orientación del UAV respecto de un sistema de referencia se puede representar de diferentes maneras.
El método más utilizado es el conocido como \textit{Ángulos de Euler} \citep{phillips2001}.
Este método considera un sistema de tres ejes coordenados ortogonales fijo en el mapa, por ejemplo el sitio de despegue, y otro sistema de referencia móvil respecto del primero pero fijo al UAV, generalmente coincidente con su centro de masa.
Se considera al eje x apuntado al frente del vehículo, el eje y ortogonal al anterior apuntando a la izquierda.
Finalmente, el eje z completando el triedro de referencia, ortogonal a los dos anteriores apuntando hacia arriba.

En este contexto, los ángulos de Euler son una secuencia de rotaciones alrededor de estos ejes para determinar la orientación del sistema de referencia de UAV respecto del sistema de referencia del donde está el origen, el cual se conocido como mapa o mundo.
El ángulo de rotación alrededor del eje x es conocido como \textit{roll} y es representado por la letra griega phi ($\phi$).
El ángulo alrededor del eje y se conoce como \textit{pitch} y se representa con la letra theta ($\theta$).
Y finalmente el ángulo de rotación alrededor del eje z es conocido como \textit{yaw} y se representa con la letra psi ($\psi$).

Otro método para representar orientación respecto de un sistema de referencia es a partir de \textit{cuaterniones} \citep{hamilton1844}.
Se pueden considerar a los cuaterniones como extensiones de los números complejos, en donde cada uno consiste de un número escalar y tres componentes imaginarias, comúnmente denotadas con i, j y k.

Si bien los cuaterniones son más difíciles de interpretar como representación de orientación, tienen ventaja sobre los ángulos de Euler ya que estos últimos sufren de un problema conocido como \textit{gimbal lock} donde, en determinada combinación de ángulos se pierde el capacidad de determinar la orientación y por consiguiente controlar el vehículo.

En este trabajo se usan los ángulos de Euler para representar los distintos sistemas de referencia, pero en bajo nivel la estimación de orientación se realiza mediante cuaterniones.

%en tres grados de libertad (ángulos de roll, pitch y yaw)

% Para automatizar la estabilización y navegación, estimación y esquemas de control adecuados son necesarios. Para ello es necesario aplicar algoritmos preparados para calculados en unidades de procesamiento de pequeñas dimensiones, capaces de filtrar la información obtenida por sensores de bajo costo y alto ruido, y obtener una estimación de posición precisa y en tiempo real

\section{Sensores}

%En términos de hardware, un controlador de vuelo es esencialmente un micro controlador, pero tiene sensores específicos a bordo.
Para poder estabilizarse en el aire un UAV necesita determinar su orientación para aplicar la potencia justa a cada motor.


La orientación puede ser determinada usando un detector de inclinación mediante un análisis trigonométrico, sin embargo debido al ruido generado por los motores de los UAV es usual combinarlo con otros sensores en un proceso llamado fusión sensorial.
La fusión puede realizarse mediante distintos métodos como redes neuronales, lógica difusa o filtros bayesianos \citep{hall2004,appriou2014,liggins2017}.

Los sensores más usados para determinar la orientación de un UAV son: acelerómetros, giróscopos y magnetómetros.
En ocasiones también se utilizan sensores para determinar la altura del vehículo, como ser sensores de rango láser o barómetros.

%Como requerimiento mínimo, un controlador de vuelo debe incluir un giroscopio de tres ejes, pero debido a que esto no es suficiente para nivelarse automáticamente, es necesario incluir un grupo de diferentes sensores para poder caracterizar la posición del vehículo.

\subsection{Acelerómetros}
Como su nombre implica, los acelerómetros miden la aceleración lineal del vehículo que lo transporta.
Existen de diferentes tecnologías~\citep{fraden2010} pudiendo incluir los tres ejes de medición (\textit{x}, \textit{y} y \textit{z}) en el mismo circuito integrado.
En los acelerómetros digitales, la unidad de medida de la salida es generalmente en \textit{g}, valor que representa $9.81\mathrm{m/s^2}$.
La salida de un acelerómetro puede ser integrada dos veces para obtener la posición estimada, aunque su precisión es baja debido a pérdidas inherentes a la operación matemática, lo que provoca un error llamado \textit{drift}, o deriva.
La característica más importante de un acelerómetro de tres ejes es que en reposo puede detectar la aceleración gravedad, y por lo tanto, pueden determinar donde es \textit{abajo}.
Esto es posible ya que permite determinar la orientación de un vector llamado usualmente \textit{Vector Gravedad}, normal a la superficie terrestre, respecto de vehículo que lleva el acelerómetro.
Este vector juega un papel fundamental en la estabilización del vehículo.

\subsection{Giróscopo}
El giróscopo mide la relación de cambio angular en los mismos tres ejes.
Esta medida, también llamada velocidad angular, es normalmente medida en grados por segundo.
Dado que esta medición representa una razón de cambio, es un valor relativo y no da información absoluta de ángulos.
Para obtener un valor absoluto es posible integrar la medición, lo cual genera un error de deriva análogo al provocado por la integración del acelerómetro.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{imu_axes}
	\caption{Ejes del Giróscopo (Izq.) y Acelerómetro (Der.)}
	\label{fig:imu_axes}
\end{figure}

\subsection{Magnetómetro}
El magnetómetro es capaz de medir el campo magnético de la Tierra y es utilizado para determinar la dirección del rumbo del UAV con respecto al norte magnético.
Este sensor puede ser utilizado para contrarrestar la deriva producida por la integración del giróscopo.

\subsection{Barómetro}
Los barómetros son sensores capaces de medir la presión atmosférica.
Dado que la presión atmosférica varía según la altura respecto del nivel del mar, un sensor de presión puede ser utilizado para obtener la altura del UAV con una precisión aceptable a distancias elevadas del suelo.

%Dado que el vehículo no cuenta con sistemas de GPS, es necesario utilizar un sensor de presión para determinar la altura en situaciones donde la altura supera el rango de trabajo del sensor láser.
Es importante tener en cuenta que las perturbaciones en el aire producidas por las hélices pueden afectar las mediciones del barómetro.
De ser posible, es recomendable protegerlo físicamente para que no quede expuesto a corrientes de aire producidas por la propulsión.

\subsection{Láser}
Dado que el sensor de presión no puede determinar cuán lejos del suelo se encuentra el vehículo, ya que mide la distancia absoluta hasta el nivel del mar, es necesario contar con un sensor capaz de detectar cuan lejos del suelo se encuentra.
Es por esto que se utiliza un medidor láser orientado hacia abajo, el cual informa, con precisión suficiente, la distancia hacia el suelo o cualquier obstáculo que se encuentre debajo del vehículo.

\newpage
\section{Filtros Bayesianos}

Un sistema, planta o proceso se puede modelar por una sucesión de variables de estado $\vect{x}_k$ llamado \textit{vector de estados} del sistema, con $k$ representando la sucesión de instantes temporales~\citep{simon2006}.
La evolución de estas variables de estado de un instante de tiempo a otro se pueden modelar como
\begin{align}
  \vect{x}_k = f_k(\vect{x}_{k-1}, \vect{w}_{k-1})
  \label{equ:bayes-transition}
\end{align}
donde la función de transición del sistema $f_k(\cdot)$ puede depender o no del instante $k$, y $\vect{w}_{k-1}$ es un vector de ruido blanco con media cero y covarianza $Q_{k-1}$ conocida, lo que puede escribirse como
\begin{align}
  E\{\vect{w}_{k-1}\} = 0 \quad \text{y} \quad E\{\vect{w}_{k-1} \vect{w}^T_{k-1}\} = Q_{k-1}
\end{align}
en donde $\vect{w}_{k-1}$ tiene la misma dimensión $n$ que el vector de estados y $Q_{k-1}$ es una matriz de $n\times n$.

En general, el estado del sistema no puede ser observado de forma directa sino a través de mediciones parciales y posiblemente perturbadas por ruido de diversas fuentes.
Se puede describir esta situación modelando la relación entre el estado del sistema y la sucesión de observaciones $\vect{z}_k$ de dimensión $m$ como
\begin{align}
  \vect{z}_k = h_k(\vect{x}_k, \vect{v}_k)
  \label{equ:bayes-observation}
\end{align}
donde la función de observación $h_k(\cdot)$ también puede depender del instante $k$, por ejemplo en sistemas con sensores de múltiples tasas de muestreo \citep{gibbs2011}.
El ruido $\vect{v}_k$ también es un vector de ruido blanco con media cero y covarianza conocida $R_{k-1}$
\begin{align}
  E\{\vect{v}_{k-1}\} = 0 \quad \text{y} \quad E\{\vect{v}_{k-1} \vect{v}^T_{k-1}\} = R_{k-1}
\end{align}
en donde $\vect{v}_{k-1}$ tiene dimensión $m$ y $R_{k-1}$ $m\times m$.

Entonces, suponiendo conocido el modelo de un sistema, incluyendo el modelo de cada sensor disponible y una parametrización del ruido que puede afectar a la planta y a las mediciones, se llaman \textit{filtros bayesianos} a los algoritmos secuenciales que pueden determinar la densidad de probabilidad el estado del sistema $p(\vect{x}_k|\vect{z}_{1:k})$ dadas todas las lecturas de los sensores hasta el tiempo $k$ utilizando recursivamente la ecuación de predicción
\begin{align}
  p(\vect{x}_{k}|\vect{z}_{1:k-1}) = \int p(\vect{x}_{k}|\vect{x}_{k-1}) p(\vect{x}_{k-1}|\vect{z}_{1:k-1})d \vect{x}_{k-1}
  \label{equ:bayes-apriori-state-summary}
\end{align}
y luego la ecuación de actualización
\begin{align}
  p(\vect{x}_{k}|\vect{z}_{1:k}) = \frac{p(\vect{z}_{k}|\vect{x}_{k}) p(\vect{x}_{k}|\vect{z}_{1:k-1})}{p(\vect{z}_k|\vect{z}_{1:k-1})} \enspace .
  \label{equ:bayes-update-summary}
\end{align}

\subsection{Filtro de Kalman}
\label{cap:control:kalman}
Si $f_k(\cdot)$ y $h_k(\cdot)$ pueden representarse con matrices $F_k$ y $H_k$ respectivamente formando un sistema lineal, y además $\vect{w}_{k-1}$ y $\vect{v}_k$ pueden considerarse ruido aditivo, se puede escribir el modelo del sistema dado por \eqref{equ:bayes-transition} y \eqref{equ:bayes-observation} como
\begin{align}
  \vect{x}_k &= F_k \vect{x}_{k-1} + \vect{w}_{k-1} \enspace, \\
  \vect{z}_k &= H_k \vect{x}_k + \vect{v}_k \label{equ:kf-linear-observation}
\end{align}
donde, $F_k$ de $n\times n$ es la \textit{matriz de transición de estado} y  $H_k$ de $m \times n$ es la \textit{matriz de observación}~\citep{kalman1960}.
En estas condiciones se considera que el sistema posee una distribución gaussiana, es decir que el vector de estados se considera una variable aleatoria $\vect{x}$ que puede ser representada con una función normal multivariada cuyos parámetros son la media $\hat{\vect{x}}$ y matriz de covarianza $P$ de $n \times n$.

Con todo esto, la estimación secuencial bayesiana dada por las ecuaciones \eqref{equ:bayes-apriori-state-summary} y \eqref{equ:bayes-update-summary} es conocida como \textit{Filtro de Kalman} \citep{kalman1960} y cuya solución en estas condiciones es cerrada y óptima bajo cualquier criterio de minimización~\citep{maybeck1982}.

En \citep{paz2016thesis} se puede encontrar el desarrollo completo del algoritmo propuesto por Kalman donde se demuestra que aplicando recursivamente las ecuaciones
\begin{align}
  \hat{\vect{x}}^{-}_{k} &=  F_k \hat{\vect{x}}_{k-1} \label{equ:kf-state-prediction-summary}\\
  P^{-}_{k} &= F_k P_{k-1} F_k^T + Q_{k-1} \label{equ:kf-covariance-prediction-summary}
\end{align}
en la etapa de predicción para determinar el estado del sistema y su covarianza a priori y luego, en la etapa de actualización las ecuaciones
\begin{align}
  K_k &= P^{-}_{k} H_k^T (H_k P^{-}_{k} H_k^T + R_k)^{-1} \label{equ:bayes-kalman-gain-summary} \\
  \hat{\vect{x}}_{k} &=  \hat{\vect{x}}^{-}_{k} + K_k ( \vect{z}_k - H_k \hat{\vect{x}}^{-}_{k} ) \label{equ:bayes-state-update-summary} \\
  P_{k} &= (I - K_k H_k ) P^{-}_{k} (I-K_k H_k)^T + K_k R_k K_k^T  \label{equ:bayes-state-covariance-update-summary}
\end{align}
se puede obtener el estado estimado $\hat{\vect{x}}_{k}$ y su matriz de covarianza $P_{k}$.

\subsection{Filtro extendido de Kalman}
Por lo general, las restricciones impuestas para el desarrollo del filtro Kalman vistas en la Sección~\ref{cap:control:kalman} no se pueden sostener, debido principalmente a que las funciones que modelan el sistema no son lineales.
El problema principal que surge de este caso es que no se puede asegurar que el estado estimado tenga distribución normal, como en la versión lineal del filtro de Kalman.
No obstante, se pueden obtener estimaciones aproximadas, las cuales son consideradas subóptimas, que pueden ser parametrizadas como una distribución normal.
El algoritmo que realiza esta estimación aproximada es conocido como \textit{Filtro Extendido de Kalman}.

Por ejemplo, si el sistema bajo análisis se puede escribir como
\begin{align}
  \vect{x}_k &= f_{k-1}(\vect{x}_{k-1}) + \vect{w}_{k-1} \enspace , \label{equ:ekf_process} \\
  \vect{z}_k &= h_{k}(\vect{x}_k) + \vect{v}_k \enspace  , \label{equ:ekf_obs}
\end{align}
con $f_{k-1}(\cdot)$ y $h_{k}(\cdot)$ funciones no lineales pero diferenciables, se pueden aproximar las mismas mediante serie de Taylor haciendo genéricamente
\begin{align}
  f(\vect{x}) = f(\hat{\vect{x}}) + J f(\hat{\vect{x}})(\vect{x}-\hat{\vect{x}}) + \mathcal{O}(\abs{\x - \hat{\x}}^2)
\end{align}
donde
\begin{align}
  Jf(\hat{\vect{x}}) =
  \begin{bmatrix}
    \partial_{x_1} f_1(\hat{\vect{x}}) &
    \partial_{x_2} f_1(\hat{\vect{x}}) &
    \dots &
    \partial_{x_n} f_1(\hat{\vect{x}})  \\
    \partial_{x_1} f_2(\hat{\vect{x}}) &
    \partial_{x_2} f_2(\hat{\vect{x}}) &
    \dots &
    \partial_{x_n} f_2(\hat{\vect{x}})  \\
    \vdots & & \ddots & \vdots \\
    \partial_{x_1}f_m(\hat{\vect{x}}) &
    \partial_{x_2}f_m(\hat{\vect{x}}) &
    \dots &
    \partial_{x_n} f_m(\hat{\vect{x}})  \\
  \end{bmatrix}
\end{align}
es el \textit{Jacobiano de la función} $f$, con $\partial_{x_i}(\cdot) = \cfrac{\partial (\cdot)}{\partial x_i} \enspace .$

En estas condiciones, se puede demostrar que el filtro extendido de Kalman calcula una estimación aproximada del estado $\hat{\vect{x}}_{k|k}$ con covarianza $P_{k|k}$, aplicando recursivamente las ecuaciones
\begin{align}
  \hat{\vect{x}}_{k| k-1} &= f_{k-1}(\hat{\vect{x}}_{k-1| k-1}) \label{equ:ekf_state_prediction} \enspace , \\
  \widehat{F}_{k-1} &= Jf_{k-1} (\hat{\vect{x}}_{k-1| k-1}) = \left. \frac {\partial f_{k-1}}{\partial \vect{x}}\right|_{\hat{\vect{x}}_{k-1}} \label{equ:ekf_state_jacobian}\enspace ,\\
  P_{k| k-1} &= Q_{k-1} + \widehat{F}_{k-1}P_{k-1| k-1}\widehat{F}^T_{k-1} \label{equ:ekf_covariance_prediction} \enspace ,
\end{align}
en la etapa de predicción, para luego en la etapa de actualización aplicar
\begin{align}
  \hat{\vect{z}}_{k| k-1} &= h_{k}(\hat{\vect{x}}_{k| k-1}), \\
  \widehat{H}_{k} &= Jh_{k} (\hat{\vect{x}}_{k|k-1}) =  \left. \frac {\partial h_{k}}{\partial \vect{x}}\right|_{\hat{\vect{x}}_k^-} \label{equ:ekf-observation-jacobian} \enspace ,\\
  K_k &=P_{k| k-1}\widehat{H}^T_{k} \big(R_{k}+\widehat{H}_{k}P_{k| k-1}\widehat{H}^T_{k}\big)^{-1} \label{equ:ekf_kalman_gain} \enspace ,\\
  \hat{\vect{x}}_{k| k} &=\hat{\vect{x}}_{k| k-1}+K_k(\vect{z}_{k}-\hat{\vect{z}}_{k| k-1}) \label{equ:ekf_state_update} \enspace , \\
  P_{k| k} &=\big(I-K_k\widehat{H}_{k}\big)P_{k| k-1} \label{equ:ekf_covariance_error_update} \enspace ,
\end{align}
siempre que las funciones $f$ y $h$ sean diferenciables y mientras que las condiciones iniciales $\hat{\vect{x}}_{0| 0}$ y $P_{0| 0}$ sean conocidas.

\subsection{Implementación del EKF}
Para el caso de los UAV, se puede utilizar como modelo del sistema a las ecuaciones cinemáticas del vehículo.
En este caso, el estado que a estimar es la orientación de UAV.
Dependiendo de que representación de orientación se utilice, será el tamaño del vector de estados.
Como se mencionó anteriormente, el uso de cuaterniones para la representación de la orientación tiene ventajas matemáticas y numéricas respecto de los ángulos de Euler.
El estado a estimar del sistema será entonces el cuaternión que representa la orientación del vehículo, por lo que el vector de estados queda
\begin{align}
  \vect{x} =
  \begin{bmatrix}
    q_0 & q_1 & q_2 & q_3
  \end{bmatrix}^T
  \label{equ:orientation-state}
\end{align}
formado por los elementos del cuaternión unitario que representa la orientación del UAV, por lo que la \textit{ecuación de proceso} del sistema se puede obtener a partir de la evolución temporal del cuaternion unitario.

\subsubsection{Etapa de predicción}
La etapa de predicción consiste en usar el modelo cinemático para tratar de estimar el desplazamiento del cuaternión.
Como entrada del modelo cinemático se utilizan las lecturas obtenidas del giróscopo
\begin{align}
\vect{\omega}(t) =
\begin{bmatrix}
  \omega_x & \omega_y & \omega_z
\end{bmatrix}^T \enspace .
\end{align}

Los cambios en la orientación de este cuerpo respecto del tiempo se pueden expresar mediante la derivada temporal del cuaternión unitario
\begin{align}
  \dot{\vect{q}} = \frac{1}{2} \vect{q} \otimes
  \begin{bmatrix}
    0 \\ \vect{\omega}(t)
  \end{bmatrix}
  \label{equ:time-derivative-cuaternion-product}
\end{align}
desarrollada en \citep{vathsal1991}.

En \citep{paz2016thesis} se muestra como el producto de cuaterniones puede ser escrito como un producto matriz-vector, por lo que \eqref{equ:time-derivative-cuaternion-product} quedaría
\begin{align}
  \dot{\vect{q}} = \frac{1}{2} \Omega(\vect{\omega}(t)) \vect{q}
  \label{equ:time-derivative-dot-product}
\end{align}
donde
\begin{align}
  \Omega(\vect{\omega}(t)) =
  \begin{bmatrix}
    0 & -\omega_x & -\omega_y & -\omega_z\\
    \omega_x & 0 & \omega_z & -\omega_y\\
    \omega_y & -\omega_z & 0 & \omega_x\\
    \omega_z & \omega_y & -\omega_x & 0
  \end{bmatrix}
  \label{equ:Omega-omega}
\end{align}
es la matriz antisimétrica asociada al vector de velocidades angulares $\vect{\omega}$.

Se puede demostrar que la solución aproximada en tiempo discreto de \eqref{equ:time-derivative-dot-product} queda
\begin{align}
  \vect{q}_k = \left( \frac{1}{2} \Omega(\vect{\omega}) \Delta t + {I} \right) \vect{q}_{k-1} \enspace .
  \label{equ:time-derivative-discretization}
\end{align}

Entonces, dado que el vector de estados $\vect{x}_k$ en el tiempo  es el cuaternión que representa la orientación, se puede escribir
\begin{align}
  \vect{x}_k = F_k \vect{x}_{k-1}
\end{align}
con
\begin{align}
  F_k = \left( \frac{1}{2} \Omega(\vect{\omega}) \Delta t + {I} \right)
  \label{equ:matrix-F}
\end{align}
como la matriz de transición de estados.
Debido a que \label{equ:matrix-F} es lineal respecto del vector de estados, no es necesario linealizarla con \eqref{equ:ekf_state_jacobian}.

\subsubsection{Etapa de actualización}
Luego, con cada lectura de los acelerómetros se ejecuta la etapa de actualización de la medición en donde se corrige el valor estimado del cuaternión basándose en la predicción del vector gravedad.

Si se considera al vector gravedad en el sistema de referencia de navegación
\begin{align}
  \vect{g}^n =
  \begin{bmatrix}
    0 & 0 & g
  \end{bmatrix}^T
\end{align}
siendo $g$ la aceleración debida a la gravedad, desde el sistema de referencia del sensor la lectura ideal del acelerómetro será
\begin{align}
  \vect{g}^b = R^b_n \vect{g}^n
  \label{equ:gravity-body}
\end{align}
con $\vect{g}^b$ la acción de la gravedad \textit{vista} desde el UAV.

En \citep{phillips2001} se muestra que la matriz de cambio de marco de referencia $R^b_n$ en función de un cuaternión unitario $\vect{q}=[q_0,q_1,q_2,q_3]^T$ es
\begin{align}
  R^b_n =
  \begin{bmatrix}
    q_0^2+q_1^2-q_2^2-q_3^2 & 2(q_1q_2+q_3q_0) & 2(q_1q_3-q_2q_0)\\
    2(q_1q_2-q_3q_0) & q_0^2-q_1^2+q_2^2-q_3^2 & 2(q_2q_3+q_1q_0)\\
    2(q_1q_3+q_2q_0) & 2(q_2q_3-q_1q_0) & q_0^2-q_1^2-q_2^2+q_3^2
  \end{bmatrix}
  \label{equ:orientation-quaternion-rotation}
\end{align}
por lo que la \eqref{equ:gravity-body} se simplifica a
\begin{align}
  \vect{g}^b = g
  \begin{bmatrix}
    2(q_1q_3-q_2q_0)\\
    2(q_2q_3+q_1q_0)\\
    q_0^2-q_1^2-q_2^2+q_3^2
  \end{bmatrix}
  \label{equ:orientation-gravity-prediction}
\end{align}
debido a que el vector gravedad $\vect{g}^n$ solo tiene la componente en el eje $z$ distinta de cero.
La ecuación \eqref{equ:orientation-gravity-prediction} es considerada como modelo de observación $H_k$ del acelerómetro a bordo del UAV respecto del cuaternión unitario que representa la orientación del vehículo.

Debido a que $H_k$ no es lineal respecto del vector de estados, debe linealizarse con \eqref{equ:ekf-observation-jacobian}, por lo que la matriz de observación del acelerómetro queda
\begin{align}
  H_{ak} =
  \begin{bmatrix}
    -2q_2 &  2q_3 & -2q_0 & 2q_1 \\
    2q_1 &  2q_0 &  2q_3 & 2q_2 \\
    2q_0 & -2q_1 & -2q_2 & 2q_3
  \end{bmatrix} \enspace .
  \label{equ:orientation-accel-measurement-prediction-Jacobian}
\end{align}

Para el caso del magnetómetro, las lecturas del mismo en el marco de referencia de navegación en caso de estar alineado con el Norte, serían
\begin{align}
  \vect{m}^n_H =
  \begin{bmatrix}
    m_N & 0 & 0
  \end{bmatrix}^T
\end{align}
La lectura de un magnetómetro ideal en estas condiciones es
\begin{align}
  \vect{m}^b_H = R^b_n \vect{m}^n_H
  \label{equ:mag-field-body}
\end{align}
con $\vect{m}^b_H$ siendo la proyección del vector campo magnético sobre el plano de navegación \textit{vista} desde el UAV.
Debido a que solo el componente $m_N$ de $\vect{m}^n_H$ es distinto de cero y usando \eqref{equ:orientation-quaternion-rotation} como matriz de rotación, se obtiene
\begin{align}
  \vect{m}^b_H =
  \begin{bmatrix}
    q_0^2+q_1^2-q_2^2-q_3^2\\
    2(q_1q_2-q_3q_0)\\
    2(q_1q_3+q_2q_0)
  \end{bmatrix}
  \label{equ:orientation-mag-measurement-prediction}
\end{align}
considerada como la ecuación que modela la observación del magnetómetro a bordo del UAV respecto del cuaternión unitario que representa la orientación del vehículo.

De la misma forma, que para el acelerómetro, esta matriz no es lineal respecto del vector de estados, por lo que debe linealizarse.
El Jacobiano de la ecuación de observación del magnetómetro \eqref{equ:orientation-mag-measurement-prediction} queda
\begin{align}
  H_{mk} =
  \begin{bmatrix}
    2q_0 & 2q_1 & -2q_2 & -2q_3 \\
    -2q_3 & 2q_2 &  2q_1 & -2q_0 \\
    2q_2 & 2q_3 &  2q_0 &  2q_1
  \end{bmatrix} \enspace .
  \label{equ:orientation-magfield-prediction-Jacobian}
\end{align}

En resumen, la implementación del filtro extendido de Kalman consiste en aplicar recursivamente lo siguiente:
\begin{enumerate}
  \item Con cada nueva lectura del giróscopo
\begin{align}
  \hat{\vect{x}}^{-}_{k} &=  F_k \hat{\vect{x}}_{k-1} \\
  P^{-}_{k} &= F_k P_{k-1} F_k^T + Q_{k-1}
\end{align}
donde
\begin{align}
  F_k = \left( \frac{1}{2} \Omega(\vect{\omega}) \Delta t + {I} \right)
\end{align}
con
\begin{align}
  \Omega(\vect{\omega}(t)) =
  \begin{bmatrix}
    0 & -\omega_x & -\omega_y & -\omega_z\\
    \omega_x & 0 & \omega_z & -\omega_y\\
    \omega_y & -\omega_z & 0 & \omega_x\\
    \omega_z & \omega_y & -\omega_x & 0
  \end{bmatrix}
\end{align}

  \item Con cada nueva lectura del acelerómetro o el magnetómetro
\begin{align}
  K_k &= P^{-}_{k} H_{k}^T (H_k P^{-}_{k} H_{k}^T + R_k)^{-1} \\
  \hat{\vect{x}}_{k} &=  \hat{\vect{x}}^{-}_{k} + K_k ( \vect{z}_k - H_{k} \hat{\vect{x}}^{-}_{k} )\\
  P_{k} &= (I - K_k H_{k} ) P^{-}_{k} (I-K_k H_{k})^T + K_k R_k K_k^T
\end{align}
con $H_k$ correspondiendo a
\begin{align}
  H_{ak} =
  \begin{bmatrix}
    -2q_2 &  2q_3 & -2q_0 & 2q_1 \\
    2q_1 &  2q_0 &  2q_3 & 2q_2 \\
    2q_0 & -2q_1 & -2q_2 & 2q_3
  \end{bmatrix} \enspace .
  \label{equ:orientation-accel-measurement-prediction-Jacobian}
\end{align}
para el caso del acelerómetro o
\begin{align}
  H_{mk} =
  \begin{bmatrix}
    2q_0 & 2q_1 & -2q_2 & -2q_3 \\
    -2q_3 & 2q_2 &  2q_1 & -2q_0 \\
    2q_2 & 2q_3 &  2q_0 &  2q_1
  \end{bmatrix}
\end{align}
para el caso del magnetómetro.

\end{enumerate}

%La disponibilidad de mejores micro procesadores ha permitido la implementación de algoritmos matemáticos más avanzados para la estimación de la orientación, velocidad y posición de los vehículos en vuelo.
%El filtro EKF, o Filtro de Kalman Extendido por sus siglas en inglés\footnote{Extended Kalman Filter}, utiliza las mediciones de velocidad angular, aceleración, presión atmosférica, orientación magnética y altura para estimar la actitud del vehículo volador. 

%La ventaja del EKF sobre filtros complementarios más simples se centra en la fusión de todas mediciones disponibles para una mejor discriminación y rechazo de mediciones con errores significativos, lo que torna al vehículo menos susceptible a fallas que afecten a un solo sensor.

%Otra característica del algoritmo EKF es que es capaz de detectar errores en las mediciones del magnetómetro y estimar el campo magnético real, siendo menos sensible a errores de calibración que los algoritmos FCM\footnote{Fuzzy Clustering \url{https://en.wikipedia.org/wiki/Fuzzy_clustering}} e INAV\footnote{Navigation Enabled Flight Control \url{https://github.com/iNavFlight/inav}}.


%\begin{figure}[h]
	%\centering
	%\includegraphics[width=1\textwidth]{kalman_output}
	%\caption{Salida del filtro Kalman. Valores de roll, pitch y yaw.}
	%\label{fig:kalman_output}
%\end{figure}

\section{Controlador PID}
\label{sec:PID}

Un controlador proporcional-integral-derivativo, normalmente abreviado \textit{PID}, es un mecanismo de retroalimentación de bucle de control genérico.
Un controlador PID calcula un valor de ``error" como la diferencia entre una variable de proceso medida y un punto de ajuste deseado e intenta minimizar el error ajustando las entradas. El diagrama en bloques de dicho controlador se observa en la Fig. \ref{fig:controlador_pid}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{pid}
	\caption{Diagrama en bloques de un controlador PID.}
	\label{fig:controlador_pid}
\end{figure}


A falta de conocimiento del proceso subyacente como es este caso, la aplicación de un controlador PID es la mas adecuada.
Sin embargo, para un mejor rendimiento, los parámetros PID utilizados en el cálculo deben ajustarse de acuerdo con la naturaleza del sistema.
El cálculo del controlador PID, definido por la ecuación \ref{ecuac:pid}, involucra tres parámetros separados: el proporcional \textit{$K_p$}, el integral \textit{$K_i$} y el derivativo \textit{$K_d$}, también denominados \textit{P}, \textit{I} y \textit{D} respectivamente. 

Al ajustar las tres constantes del algoritmo, el controlador puede proporcionar una acción de control específica. 

\begin{align}
u(t)=K_{\text{p}}e(t)+K_{\text{i}}\int _{0}^{t}e(t')\,dt' +K_{\text{d}}{\frac {de(t)}{dt}} 
\label{ecuac:pid}
\end{align}


\subsection{Constantes Involucradas}
\subsubsection{Término Proporcional}
El término proporcional ajusta la señal de control basado en la magnitud del error en el tiempo presente.
Esto significa que a mayor error instantáneo, mayor será la señal de control aplicada en ese momento.

De forma ilustrativa, si el control se aplica a la velocidad de un avión que se desplaza a $30kmph$, siendo la velocidad objetivo $50kmph$ el error sería de $20kmph$ y el término añadiría $20\times K_p$ a la señal de salida, incrementando efectivamente el acelerador.

Cuanto mayor sea la diferencia entre la velocidad actual y el objetivo de $50kmph$, mayor será el incremento aplicado, y cuanto menor sea la diferencia, menor el incremento.

\subsubsection{Término Integral}
El término integral modifica la señal de control proporcionalmente a la magnitud del error acumulado a través del tiempo.
A mayor error acumulado, una mayor señal es aplicada.
Se observa que un error negativo puede cancelar un error positivo.

En el ejemplo de la aeronave, si esta estuvo volando a $30kmph$ por 12 segundos, mientras que el objetivo es $50kmph$, se aplicaría $(20\times 12) \times K_i$ sobre la señal de salida, incrementando el acelerador.
A mayor tiempo debajo del objetivo, mayor será el aporte de este término.

Mientras que el término proporcional ajusta la salida basada en el error presente, el término integral lo hace basado en los errores anteriores.

\subsubsection{Término Diferencial}
El término diferencial ajusta la señal de control proporcional a la tasa de cambio del error con respecto al tiempo, es decir, según la velocidad de variación del error.

Utilizando el ejemplo de la aeronave, teniendo un error de $20kmph$ y una aceleración tal que la velocidad aumente $5kmph$ por segundo, el error disminuye $5kmph$ por segundo, por lo que el término diferencial del error es $-5$.
El valor del término derivativo aplicaría $-5\times K_d$ a la señal de salida, reduciendo así el acelerador.
Mientras mas rápido se acerque al objetivo de $50kmph$, menor la señal de salida.

Este efecto permite disminuir la aceleración a medida que se acerca al objetivo, evitando sobrepasarlo.
El término diferencial ajusta la salida basado en una predicción del error futuro.
Saber cuan rápido varía el error permite estimar como será este en un futuro instantáneo.


\subsection{Calibración}
\subsection{Controlador P}


Se desea que el controlador PID haga volar la aeronave anteriormente mencionada a $50kmph$, a partir de $25kmph$.
De la explicación anterior sobre el término proporcional puede parecer que el $K_p$ es suficiente para que el controlador funcione.
Entonces, se establece $K_p$ en algún valor positivo, $K_i$ en 0 y $K_d$ en 0 para que la integral y los términos diferenciales desaparezcan.
La aeronave se comportaría como se muestra en la Fig \ref{fig:pid_p}; se acelera y se estabiliza a una velocidad mayor, pero nunca llega a la meta de $50kmph$.

La razón por la que esto sucede es debido al arrastre y la forma en que funciona el término proporcional. 
Si se viaja a $50kmph$, la señal de salida al acelerador sería $(50-50)\times K_p = 0$, porque el error sería cero. 
El avión no puede volar a $50kmph$ con el acelerador a $0$ debido al arrastre, y por lo tanto, la velocidad se estabilizará en algún lugar por debajo de los $50kmph$, donde el error es distinto de cero y se está aplicando algún nivel de aceleración. 

Este error constante, distinto de cero, se denomina error de estado estacionario.
Por lo tanto, el término proporcional no es suficiente para llevarnos a nuestra velocidad deseada en este caso.

\subsection{Controlador PI}

El objetivo del término integral es reducir el error de estado estacionario a cero para que se pueda alcanzar la meta de velocidad.
Cuanto más tiempo se tenga un error positivo, más aceleración aplicará el término integral, hasta que finalmente se alcance la velocidad deseada.
La Fig. \ref{fig:pid_pi} muestra el comportamiento de la aeronave si se establecen valores positivos en $K_p$ y $K_i$, pero se mantiene $K_d$ en cero.
Ahora el avión llega a $50 kmph$, pero sobrepasa y oscila antes de estabilizarse lentamente. 

El término integral lo llevará a la meta, pero probablemente lo sobrepasará e inducirá oscilaciones.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{PID_P}
		\caption{Controlador P}
		\label{fig:pid_p}		
	\end{subfigure}
	\begin{subfigure}[h]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{PID_PI}
		\caption{Controlador PI}
		\label{fig:pid_pi}
	\end{subfigure}
	\caption{Respuesta temporal del controlador P y controlador PI}
	\label{fig:ppi}
\end{figure}


\subsection{Controlador PID}

El objetivo del término diferencial es amortiguar las oscilaciones, por lo que permite alcanzar el objetivo de $50kmph$ con un mínimo de oscilaciones.
La Fig \ref{fig:pid_pid} muestra cómo podría comportarse la aeronave si se establecen valores correctos para las tres constantes.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{PID_PID}
	\caption{Controlador PID}
	\label{fig:pid_pid}
\end{figure}

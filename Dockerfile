FROM debian:unstable

RUN apt-get update && apt-get install eatmydata -y

# This are all the *tex* packages in my linux and it works.
# For sure most of them are not used.
RUN apt-get update && eatmydata apt-get install -y \
    preview-latex-style \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-latex-recommended \
    texlive \
    texlive-base \
    texlive-bibtex-extra \
    texlive-binaries \
    texlive-extra-utils \
    texlive-fonts-recommended \
    texlive-formats-extra \
    texlive-lang-greek \
    texlive-lang-spanish \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-latex-recommended \
    texlive-pictures \
    texlive-plain-generic \
    texlive-science \
    texlive-xetex 
